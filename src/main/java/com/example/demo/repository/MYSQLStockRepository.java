package com.example.demo.repository;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Port;
import com.example.demo.entity.Stock;
import com.example.demo.wrapper.StockWrapper;

import yahoofinance.YahooFinance;

@Repository
public class MYSQLStockRepository implements StockRepository {

	@Autowired
	private JdbcTemplate template;

	@Override
	public StockWrapper displayStockDetails(String ticker) throws IOException {
		return new StockWrapper(YahooFinance.get(ticker));
	}
	
	@Override
	public yahoofinance.Stock exploreStock(String ticker) throws IOException{
		return YahooFinance.get(ticker);
	}

	@Override
	public List<Stock> getAllStocks() {
		String sql = "SELECT * FROM history order by datetime DESC";
		return template.query(sql, new StockRowMapper());
	}
	
	@Override
	public List<Port> getAllPort() {
		String sql = "SELECT * FROM stockdetails order by netAssetValue DESC";
		return template.query(sql, new PortRowMapper());
	}
	
	@Override
	public Stock addStock(Stock stock) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		template.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO history(ticker, dateTime, volume, valuePerStock, buyOrSell) VALUES(?,?,?,?,?);"
								+ "INSERT INTO StockDetails(ticker, volume, totalPricePaid) VALUES (?,?,?) ON DUPLICATE KEY UPDATE volume=volume+?, totalPricePaid = totalPricePaid+?;",
						Statement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, stock.getTicker());
				ps.setTimestamp(2, stock.getDateTime());
				ps.setInt(3, stock.getVolume());
				ps.setDouble(4, stock.getValuePerStock());
				ps.setString(5, stock.getBuyOrSell());
				
				ps.setString(6, stock.getTicker());
				ps.setInt(7, stock.getVolume());
				ps.setDouble(8, stock.getVolume()*stock.getValuePerStock());
				
		        ps.setDouble(9, stock.getVolume());
		        ps.setDouble(10, stock.getVolume()*stock.getValuePerStock());
		        				
				return ps;
			}
		}, keyHolder);

		stock.setOrderId(keyHolder.getKey().intValue());
		return stock;
	}
	
	@Override
	public Stock sellStock(Stock stock) {
		
		String sql = "SELECT * FROM stockdetails";
		List<Port> volumes = template.query(sql, new PortRowMapper());
		boolean tickerExists = false;
		
		for(int i = 0; i < volumes.size(); i++) {
		
		if (volumes.get(i).getTicker().equals(stock.getTicker())) {
			tickerExists = true;
			if(stock.getVolume() > volumes.get(i).getVolume()) { 
				  return null;
			}
		}
		}
		
		if(tickerExists==false) {
			return null;
		}
		
	    KeyHolder keyHolder = new GeneratedKeyHolder();
		template.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	
				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO history(ticker, dateTime, volume, valuePerStock, buyOrSell) VALUES(?,?,?,?,?);"
								+ "UPDATE stockdetails SET volume = volume - ?, totalPricePaid = totalPricePaid - ? WHERE ticker = ?",
						Statement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, stock.getTicker());
				ps.setTimestamp(2, stock.getDateTime());
				ps.setInt(3, stock.getVolume());
				ps.setDouble(4, stock.getValuePerStock());
				ps.setString(5, stock.getBuyOrSell());
				
				ps.setInt(6, stock.getVolume());
				ps.setDouble(7, stock.getVolume()*stock.getValuePerStock());
				ps.setString(8, stock.getTicker());
				return ps;
			}
		}, keyHolder);
		
		String sql1 = "DELETE FROM stockdetails WHERE volume = 0";
		template.update(sql1);
	
		stock.setOrderId(keyHolder.getKey().intValue());
		return stock;
	}
	
	@Override
	public List<Port> updatePortfolio() {
		String sql = "SELECT * FROM stockdetails";				
		List<Port> tickers = template.query(sql, new PortRowMapper());
		
	    for(int i = 0; i < tickers.size(); i++) {
	    	String ticker = tickers.get(i).getTicker();
	    			
	    	KeyHolder keyHolder1 = new GeneratedKeyHolder();
			template.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection1) throws SQLException {

					PreparedStatement ps1 = connection1.prepareStatement(
							"UPDATE stockdetails SET netAssetValue = volume * ? WHERE ticker = ?",
							Statement.RETURN_GENERATED_KEYS);
					
					
					double price = 0;
					try {
						price = (YahooFinance.get(ticker).getQuote().getPrice()).doubleValue();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					ps1.setDouble(1, price);
					ps1.setString(2, ticker);
					
					return ps1;
				}
			}, keyHolder1);
		    
			KeyHolder keyHolder2 = new GeneratedKeyHolder();
			template.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection2) throws SQLException {

					PreparedStatement ps2 = connection2.prepareStatement(
							"UPDATE stockdetails SET returns = netAssetValue-totalPricePaid",
							Statement.RETURN_GENERATED_KEYS);
					
					return ps2;
				}
			}, keyHolder2);
		}
	    
	    return getAllPort();
	}
	
	@Override
	public double getSumStock() {
		List<Port> sum= updatePortfolio();
		double total=0;
		for(int i = 0; i < sum.size(); i++) {
			total=total+ sum.get(i).getnetAssetValue();
			}
		return total;
	}

	@Override
	public double getSumReturns() {
		List<Port> sum= updatePortfolio();
		double total=0;
		for(int i = 0; i < sum.size(); i++) {
			total=total+ sum.get(i).getReturns();
			}
		return total;
	}

}

class StockRowMapper implements RowMapper<Stock> {

	@Override
	public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Stock(rs.getInt("orderID"), rs.getString("ticker"), rs.getTimestamp("dateTime"),
				rs.getInt("volume"), rs.getInt("valuePerStock"), rs.getString("buyOrSell"));
	}

}

class PortRowMapper implements RowMapper<Port> {

	@Override
	public Port mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Port(rs.getString("ticker"),rs.getInt("volume"), rs.getDouble("totalPricePaid"), rs.getDouble("netAssetValue"), rs.getDouble("returns"));
	}

}